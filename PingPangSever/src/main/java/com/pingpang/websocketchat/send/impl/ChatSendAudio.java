package com.pingpang.websocketchat.send.impl;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class ChatSendAudio extends ChatSend {

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
       
		if (null != ChannelManager.getChannel(message.getAccept().getUserCode())) {
			
		    Set<String> msg=new HashSet<String>();
			msg.add("request");
			msg.add("close");
			msg.add("refuse");
			msg.add("accept");
			if(!msg.contains(message.getMsg())){
				logger.info("IP:{},用户:{},对方用户:{}多媒体错误信息{}",ctx.channel().remoteAddress(),message.getFrom().getUserCode(),message.getAccept().getUserCode(),message.getMsg());
				return;
			}
			//发送数据这里改为redis
			message.getFrom().setUserPassword("");
			message.getAccept().setUserPassword("");
			
			message.setFrom(userService.getUser(message.getFrom()));
			message.setAccept(userService.getUser(message.getAccept()));
			
			ChannelManager.getChannel(message.getAccept().getUserCode())
					.writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
			
			if("close".equals(message.getMsg())) {
				message.setCmd("3");
				message.setMsg("【视频聊天结束】");
				userMsgService.addMsg(message);
			}
			
			if("refuse".equals(message.getMsg())) {
				message.setCmd("3");
				message.setMsg("【视频聊天被拒绝】");
				userMsgService.addMsg(message);
			}
			
			if("3".equals(message.getCmd())) {
			   ChannelManager.getChannel(message.getAccept().getUserCode())
		 	   .writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
			}
	    }
	}

}
