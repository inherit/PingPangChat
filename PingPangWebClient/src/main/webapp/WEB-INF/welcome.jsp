<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>PingPang管理页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
        <script src="${httpServletRequest.getContextPath()}/layui.js" charset="utf-8"></script>
        <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
        <script type="text/javascript" src="${httpServletRequest.getContextPath()}/x-admin/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                 <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">用户数统计</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main1" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div>
                
                <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">最新一周新增用户</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main2" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div>
                
                <div class="layui-col-sm12 layui-col-md6">
                    <div class="layui-card">
                        <div class="layui-card-header">硬盘使用量</div>
                        <div class="layui-card-body" style="min-height: 280px;">
                            <div id="main4" class="layui-col-sm12" style="height: 300px;"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <script src="${httpServletRequest.getContextPath()}/echarts.min.js"></script>
        <script type="text/javascript">
        
     // 基于准备好的dom，初始化echarts实例
        // 基于准备好的dom，初始化echarts实例
      function userChat(){
    	 
    	  $.ajax({
    		    url:"${httpServletRequest.getContextPath()}/userController/welcome-index",
    		    type:"Post",
    		    async:false,//同步
    		    dataType:"json",
    		    success:function(data){
    		    	 //data=eval("('"+data+"')");
    		         //console.log(data.userDate);
    		         //console.log(data.userCount);
    		    	 var myChart1 = echarts.init(document.getElementById('main1'));

    		         // 指定图表的配置项和数据
    		         var option1 = {
    		             tooltip : {
    		                 trigger: 'item',
    		                 formatter: "{a} <br/>{b} : {c} ({d}%)"
    		             },
    		             legend: {
    		                 orient: 'vertical',
    		                 left: 'left',
    		                 data: eval(data.userDate)
    		             },
    		             series : [
    		                 {
    		                     name: '访问来源',
    		                     type: 'pie',
    		                     radius : '55%',
    		                     center: ['50%', '60%'],
    		                     data:   eval(data.userCount),
    		                     itemStyle: {
    		                         emphasis: {
    		                             shadowBlur: 10,
    		                             shadowOffsetX: 0,
    		                             shadowColor: 'rgba(0, 0, 0, 0.5)'
    		                         }
    		                     }
    		                 }
    		             ]
    		         };

    		         // 使用刚指定的配置项和数据显示图表。
    		         myChart1.setOption(option1);
    		         
    		         
    		         // 基于准备好的dom，初始化echarts实例
    		         var myChart2 = echarts.init(document.getElementById('main2'));

    		         // 指定图表的配置项和数据
    		         var option2 = {
    		             grid: {
    		                 top: '5%',
    		                 right: '1%',
    		                 left: '1%',
    		                 bottom: '10%',
    		                 containLabel: true
    		             },
    		             tooltip: {
    		                 trigger: 'axis'
    		             },
    		             xAxis: {
    		                 type: 'category',
    		                 data: eval(data.dbRegistUserDate)
    		             },
    		             yAxis: {
    		                 type: 'value'
    		             },
    		             series: [{
    		                 name:'用户量',
    		                 data: eval(data.dbRegistUserCount),
    		                 type: 'line',
    		                 smooth: true
    		             }]
    		         };


    		         // 使用刚指定的配置项和数据显示图表。
    		         myChart2.setOption(option2);

    		          // 基于准备好的dom，初始化echarts实例
    		         var myChart4 = echarts.init(document.getElementById('main4'));

    		         // 指定图表的配置项和数据
    		         var option4 = {
    		             tooltip : {
    		                 formatter: "{a} <br/>{b} : {c}%"
    		             },
    		             series: [
    		                 {
    		                     name: '硬盘使用量',
    		                     type: 'gauge',
    		                     detail: {formatter:'{value}%'},
    		                     data: [{value: 88, name: '已使用'}]
    		                 }
    		             ]
    		         };
    		         // 使用刚指定的配置项和数据显示图表。
    		         myChart4.setOption(option4);
    		         
    		    },
    		     error:function(data){
    		     console.log("获取服务器失败...");
    		    }
    		}); 
      }
        userChat();
        setInterval(userChat,5000); //指定1秒刷新一次
    </script>
    </body>
</html>