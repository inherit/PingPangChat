<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>PINGPANGCHAT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/static/css/mian.css"  media="all">
	<link rel="stylesheet" href="${httpServletRequest.getContextPath()}/css/layui.css"  media="all">
</head>
<body class="lay-blog">
		<div class="header">
			<div class="header-wrap">
				<h1 class="logo pull-left">
					<a href="index.html">
						<img src="${httpServletRequest.getContextPath()}/static/images/logo.png" alt="" class="logo-img">
						<span class="logo-img" style="background: #C1E4FF;">CHAT</span>
					</a>
				</h1>
				<form class="layui-form blog-seach pull-left" action="">
					<div class="layui-form-item blog-sewrap">
					    <div class="layui-input-block blog-sebox">
					      <i class="layui-icon layui-icon-search"></i>
					      <input type="text" name="title" lay-verify="title" autocomplete="off"  class="layui-input">
					    </div>
					</div>
				</form>
				<div class="blog-nav pull-right">
					<ul class="layui-nav pull-left">
					  <li class="layui-nav-item layui-this"><a href="index.html">首页</a></li>
					  <li class="layui-nav-item"><a href="${httpServletRequest.getContextPath()}/user/index">闲聊</a></li>
					  <li class="layui-nav-item"><a href="">关于</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="container">
					<div class="contar-wrap">
						<h4 class="item-title">
							<p>
							<i class="layui-icon layui-icon-speaker"></i>
							公告：<span>
							     <a href="https://gitee.com/0X00000000/PingPangChat/tree/2.1.0/" target="_blank">PINGPANGCHAT 2.1.0发布</a><br>
							     <a href="https://gitee.com/0X00000000/PingPangChat/tree/2.0.0/" target="_blank">PINGPANGCHAT 2.0.0发布</a><br>
							     <a href='https://gitee.com/0X00000000/PingPangChat/stargazers' target="_blank">
							        <img src='https://gitee.com/0X00000000/PingPangChat/badge/star.svg?theme=dark' alt='star'></img>
							     </a>
							     <a href='https://gitee.com/0X00000000/PingPangChat/members' target="_blank">
							        <img src='https://gitee.com/0X00000000/PingPangChat/badge/fork.svg?theme=dark' alt='fork'></img>
							     </a>
							     
							     <a href="https://gitee.com/0X00000000/PingPangChat/tree/2.0.0/" style="color: #FF5722;" target="_blank">更新日志</a>
							    </span>
							</p>
						</h4>
					</div>
			</div>
			
			<div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">
                          更新日志
                        </div>
                        <div class="layui-card-body ">
                          <ul class="layui-timeline">
						         <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-07-18</h3>
                                <p>
                                  文件存储在fastdfs服务器中，web端添加了断线重连<br>
                                </p>
                              </div>
                            </li>
                            
						      <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-06-26</h3>
                                <p>
                                  对系功能进行了拆分，分为db、server、webclient<br>
                                </p>
                              </div>
                            </li>
						      
						      <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-06-13</h3>
                                <p>
                                  修复了一些bug,群组聊天显示在线用户昵称<br>
                                </p>
                              </div>
                            </li>
						    
						    <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-05-28</h3>
                                <p>
                                  对NETTY进行集群处理<br>
                                </p>
                              </div>
                            </li>
						  
                            <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-05-08</h3>
                                <p>
                                  对用户、群组数据进行缓存处理<br>
                                </p>
                              </div>
                            </li>
                            <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-05-02</h3>
                                <p>
								 对用户信息进行持久化到数据库<br>
								 管理员后台用户数据维护(用户注册、在线用户展示、实时用户信息查看)<br>
								 最近聊天用户展示<br>
								 最近聊天信息展示<br>
								 聊天界面美化<br>
								 管理员消息广播
								</p>
                              </div>
                            </li>
                            <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-04-26</h3>
                                <p>
                                  增加了WEB录音,语音发送
                                </p>
                              </div>
                            </li>
                            <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-04-23</h3>
                                <p>
								  图片、音频、视频发送<br>
                                  图片发送的时候进行压缩、双击可以显示原图<br>
                                </p>
                              </div>
                            </li>
                            <li class="layui-timeline-item">
                              <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                              <div class="layui-timeline-content layui-text">
                                <h3 class="layui-timeline-title">2020-04-15</h3>
                                <p>
                                   搭建主体框架（未实例化到数据库）<br>
								   在线用户列表实时刷新<br>
								   消息提示、单聊、群聊的实现<br>
                                </p>
                              </div>
                            </li>
                          </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
			
		</div>
		<div class="footer">
			<p>
				<span>&copy; 2020</span>
				<span><a href="http://www.beian.miit.gov.cn/" target="_blank">豫ICP备20015528号</a></span> 
			</p>
		</div>
</body>
</html>