package com.pingpang.load.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.pingpang.load.LoadInterface;
import com.pingpang.redis.RedisPre;

/**
   * 随机数处理
 * @author dell
 */
public class LoadRandom extends LoadInterface{

	@Override
	public String getAddress() {
		return this.redisService.getRandomServer(RedisPre.SERVER_ADDRES_ZSET, 1);
	}
}
