package com.pingpang.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.service.UserMsgService;
import com.pingpang.service.UserService;
import com.pingpang.util.PageUtil;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChartUser;
import com.pingpang.websocketchat.Message;

@RestController
@RequestMapping("/msg")
public class MsgController {
 
	@Autowired
	private UserMsgService userMsgService;
	
	@Autowired
	private UserService userService;
	/**
	  * 列表页面
	 * @return
	 */
	@RequestMapping("/db-msg-list")
	public ModelAndView dbMsgList() {
		return new ModelAndView("db-msg-list");
	}
	
	/**
	 * 获取用户信息
	 */
	@ResponseBody
	@RequestMapping(value="/db-list")
	public Map<String,Object> dbMsgListData(@RequestBody Map<String,String> queryMap) throws JsonParseException, JsonMappingException, IOException{
		queryMap.putAll(PageUtil.getPage(queryMap.get("page"), queryMap.get("limit")));
		ObjectMapper mapper = new ObjectMapper(); 
		if(!StringUtil.isNUll(queryMap.get("search"))) {
			queryMap.putAll(mapper.readValue(queryMap.get("search"), Map.class));
			queryMap.remove("search");
		}
		
		queryMap.putAll(PageUtil.getPage(queryMap.get("page"), queryMap.get("limit")));
		
 		if(!StringUtil.isNUll(queryMap.get("search"))) {
 			queryMap.putAll(mapper.readValue(queryMap.get("search"), Map.class));
 			queryMap.remove("search");
 		}
 		
 		Map<String,Object>resultMap=new HashMap<String,Object>();
 		resultMap.put("code", "0");
 		resultMap.put("msg", "");
 		resultMap.put("page", queryMap.get("page"));
 		resultMap.put("limit", queryMap.get("limit"));
 		resultMap.put("count", this.userMsgService.getAllMsgCount(queryMap));
 		resultMap.put("data",this.userMsgService.getAllMsg(queryMap));
 		return resultMap;
	}
	
	/**
	  * 下线
	 * @param code
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/db-downMsg",method=RequestMethod.POST)
	@ResponseBody
	public  Map<String,Object> dbDownMsg(@RequestParam("id") String code) throws Exception {
    	if(StringUtil.isNUll(code)) {
 	    	return StringUtil.returnSucess();
 	    }	
    	
       Map<String,Object> upMsg=new HashMap<String, Object>();
  	   upMsg.put("id",code.split(",",-1));
       upMsg.put("status","0");
       this.userMsgService.upMsg(upMsg); 		
       return StringUtil.returnSucess();
    }
    
    /**
	  * 上线
	 * @param code
	 * @return
	 * @throws Exception
	 */
   @RequestMapping(value="/db-upMsg",method=RequestMethod.POST)
	@ResponseBody
	public  Map<String,Object> dbUpMsg(@RequestParam("id") String code) throws Exception {
	   if(StringUtil.isNUll(code)) {
	    	return StringUtil.returnSucess();
	    }
	   
	   Map<String,Object> upMsg=new HashMap<String, Object>();
	   upMsg.put("id",code.split(",",-1));
       upMsg.put("status","1");
       this.userMsgService.upMsg(upMsg); 		
       return StringUtil.returnSucess();	
	}
   
    /**
	   *  注销
	 * @param code
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/db-delMsg",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> dbDelMsg(@RequestParam("id") String code) throws Exception {
    	if(StringUtil.isNUll(code)) {
 	    	return StringUtil.returnSucess();
 	    }
    	 
    	Map<String,Object> upMsg=new HashMap<String, Object>();
  	    upMsg.put("id",code.split(",",-1));
        upMsg.put("status","-1");
        this.userMsgService.upMsg(upMsg); 		
        return StringUtil.returnSucess();
	}
    
    
    /**
	  * 下线
	 * @param code
	 * @return
	 * @throws Exception
	 */
   @RequestMapping(value="/db-downMsgAll",method=RequestMethod.POST)
	@ResponseBody
	public  Map<String,Object> dbDownMsgAll(@RequestParam("ids") String code) throws Exception {
	   Map<String,Object> upMsg=new HashMap<String, Object>();
	   upMsg.put("id",code.split(",",-1));
       upMsg.put("status","0");
       this.userMsgService.upMsg(upMsg); 		
       return StringUtil.returnSucess();
   }
   
   /**
	  * 上线
	 * @param code
	 * @return
	 * @throws Exception
	 */
  @RequestMapping(value="/db-upMsgAll",method=RequestMethod.POST)
	@ResponseBody
	public  Map<String,Object> dbUpUserAll(@RequestParam("ids") String code) throws Exception {
	  if(StringUtil.isNUll(code)) {
	    	return StringUtil.returnSucess();
	    }
	  Map<String,Object> upMsg=new HashMap<String, Object>();
	  upMsg.put("id",code.split(",",-1));
      upMsg.put("status","1");
      this.userMsgService.upMsg(upMsg); 		
      return StringUtil.returnSucess();
	}
  
   /**
	   *  注销
	 * @param code
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/db-delMsgAll",method=RequestMethod.POST)
	@ResponseBody
	public  Map<String,Object> dbDelUserAll(@RequestParam("ids") String code) throws Exception {
	   Map<String,Object> upMsg=new HashMap<String, Object>();
	   upMsg.put("id",code.split(",",-1));
       upMsg.put("status","-1");
       this.userMsgService.upMsg(upMsg); 		
       return StringUtil.returnSucess();
	}
	
   
    @RequestMapping(value="/getUserOldChat",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> getUserOldChat(String userCode){
    	if(StringUtil.isNUll(userCode)) {
    		return StringUtil.returnFail();
    	}
    	
    	ChartUser cu=new ChartUser();
    	cu.setUserCode(userCode);
    	cu=this.userService.getUser(cu);
    	
    	List<Map<String,String>> userOldList=this.userMsgService.getUserOldChat(cu);
    	Map<String,Object>result=StringUtil.returnSucess();
    	result.put("userList", userOldList);
	    return result;
	}
    
    @RequestMapping(value="/getUserOldMsg",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> getUserOldMsg(Message message){
    	if(null==message || StringUtil.isNUll(message.getFrom().getUserCode()) || StringUtil.isNUll(message.getAccept().getUserCode())) {
    		return StringUtil.returnFail();
    	}
    	List<Message> userOldList=this.userMsgService.getUserOldMsg(message);
    	Map<String,Object>result=StringUtil.returnSucess();
    	result.put("userOldMsg", userOldList);
	    return result;
	}
    
}
