package com.pingpang.util;

import javax.servlet.http.HttpServletRequest;

public class IPUtil {
	/** 
	 * 获得客户端真实IP地址 
	 * @[author]param[/author] request 
	 * @return 
	 */ 
	public static String getIpAddr(HttpServletRequest request) { 
	   String ip = request.getHeader("X-Forwarded-For");
	   ip = getTrueIp(ip);
		    	
	   if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	       ip = request.getHeader("Proxy-Client-IP");
	       ip = getTrueIp(ip);
	   }
	   if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	       ip = request.getHeader("WL-Proxy-Client-IP");
	       ip = getTrueIp(ip);
	   }
	   if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	       ip = request.getHeader("HTTP_CLIENT_IP");
	       ip = getTrueIp(ip);
	   }
	   if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	       ip = request.getHeader("HTTP_X_FORWARDED_FOR");
	       ip = getTrueIp(ip);
	   }
	   if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	      ip = request.getRemoteAddr();
	      ip = getTrueIp(ip);
	   }
	      return ip; 
	}


	/**
	 * 取真实客户端IP，过滤代理IP
	 * @[author]param[/author] ip
	 * @return
	 */
	public static String getTrueIp(String ip){
	   if(ip == null || "".equals(ip))return null;
	   if(ip.indexOf(",") != -1){
	       String[] ipAddr = ip.split(",",-1);
	       for(int i=0; i<ipAddr.length; i++){
	          if(isIp(ipAddr[i].trim()) && !ipAddr[i].trim().startsWith("10.")
	             && !ipAddr[i].trim().startsWith("172.16")){
	             return ipAddr[i].trim();
	          } 
	       }
	   }else{
	       if(isIp(ip.trim()) && !ip.trim().startsWith("10.")
	          && !ip.trim().startsWith("172.16"))
	          return ip.trim();
	   }
	   return null;
	}
	
	 /**
                * 判断所有的IP地址
      * @param IP
      * @return
      */
    public static boolean isIp(String IP) {
 
        if (!IP.contains(".") && !IP.contains(":")) {
            return false;
        }
        //如果是IPV4
        if (IP.contains(".")) {
            if (IP.endsWith(".")) {
                return false;
            }
            String[] arr = IP.split("\\.");
            if (arr.length != 4) {
                return false;
            }
 
            for (int i = 0; i < 4; i++) {
                if (arr[i].length() == 0 || arr[i].length() > 3) {
                    return false;
                }
                for (int j = 0; j < arr[i].length(); j++) {
                    if (arr[i].charAt(j) >= '0' && arr[i].charAt(j) <= '9') {
                        continue;
                    }
                    return false;
                }
                if (Integer.valueOf(arr[i]) > 255 || (arr[i].length() >= 2 && String.valueOf(arr[i]).startsWith("0"))) {
                    return false;
                }
            }
            return true;
        }//如果是IPV4
 
        //如果是IPV6
        if (IP.contains(":")) {
            if (IP.endsWith(":") && !IP.endsWith("::")) {
                return false;
            }
            //如果包含多个“::”，一个IPv6地址中只能出现一个“::”
            if (IP.indexOf("::") != -1 && IP.indexOf("::", IP.indexOf("::") + 2) != -1) {
                return false;
            }
 
            //如果含有一个“::”
            if (IP.contains("::")) {
                String[] arr = IP.split(":");
                if (arr.length > 7 || arr.length < 1) {//"1::"是最短的字符串
                    return false;
                }
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i].equals("")) {
                        continue;
                    }
                    if (arr[i].length() > 4) {
                        return false;
                    }
                    for (int j = 0; j < arr[i].length(); j++) {
                        if ((arr[i].charAt(j) >= '0' && arr[i].charAt(j) <= '9') || (arr[i].charAt(j) >= 'A' && arr[i].charAt(j) <= 'F')
                                || (arr[i].charAt(j) >= 'a' && arr[i].charAt(j) <= 'f')) {
                            continue;
                        }
                        return false;
                    }
                }
                return true;
            }
 
            //如果不含有“::”
            if (!IP.contains("::")) {
                String[] arr = IP.split(":");
                if (arr.length != 8) {
                    return false;
                }
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i].length() > 4) {
                        return false;
                    }
                    for (int j = 0; j < arr[i].length(); j++) {
                        if ((arr[i].charAt(j) >= '0' && arr[i].charAt(j) <= '9') || (arr[i].charAt(j) >= 'A' && arr[i].charAt(j) <= 'F')
                                || (arr[i].charAt(j) >= 'a' && arr[i].charAt(j) <= 'f')) {
                            continue;
                        }
                        return false;
                    }
                }
                return true;
            }
        }//如果是IPV6
        return false;
    }
}
